<?php
	
	class User extends Controller{

		function __construct(){
                    parent::__construct();
		}

		public function index(){
			echo "Hola, este es el metodo index del Usuario";
		}
                
                public function profile(){
                    $this->view->render(get_class($this)."/profile");
                }

		public function saludar($name){
			echo "Hola, ".$name;
		}
                public function subirImagen(){
                    
                    $rootKey = key($_FILES);
                    $nArchivos = count($_FILES[$rootKey]["name"]);
                    $archivos = [];
                    
                    for ($i = 0; $i < $nArchivos; $i++) {
                    //name
                    $archivos[$i]["name"] = $_FILES[$rootKey]["name"][$i];
                    //type
                    $archivos[$i]["type"] = $_FILES[$rootKey]["type"][$i];
                    //tmp_name
                    $archivos[$i]["tmp_name"] = $_FILES[$rootKey]["tmp_name"][$i];
                    //error
                    $archivos[$i]["error"] = $_FILES[$rootKey]["error"][$i];
                    //size
                    $archivos[$i]["size"] = $_FILES[$rootKey]["size"][$i];
                    }
                    
                    require "libs/FileUploader.php";
                    foreach ($archivos as $archivo) {
                        FileUploader::subir($archivo,"public/img/");
                    }

                     
                }
                
                public function signIn(){
                    
                    if( isset($_POST["username"]) && isset($_POST["password"])){
                        $response = $this->model->select('*',"username = '".$_POST["username"]."'");
                        $response = $response[0];
                        if($response['password'] == $_POST["password"]){
                            $this->createSession();
                            echo 1;
                        }
                    }
                }
                
                public function signUp() {
                            
                    if( isset($_POST["name"])  && isset($_POST["username"]) &&
                        isset($_POST["email"]) && isset($_POST["password"])){
                        
                        if( $_POST["name"] != ''  && $_POST["username"] != '' &&
                        $_POST["email"] != '' && $_POST["password"] != ''){
                            
                            
                            $data["name"] = $_POST["name"];
                            $data["username"] = $_POST["username"];
                            $data["email"] = $_POST["email"];
                            $data["password"] = $_POST["password"];

                            print($this->model->signUp($data));
                        
                        }else{
                            print(0);
                        }
                        
                    }else{
                        print(0);
                    }
                    
                }

		public function createSession(){
			Session::setValue('U_NAME',$_POST['username']);
			Session::setValue('U_ID',1);
		}

		public function destroySession(){
			Session::destroy();
                        header('location:'.URL);
		}
	}

?>