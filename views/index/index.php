<!Doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Session</title>
		<script src="<?php echo URL;?>/public/js/jquery-1.11.0.min.js"></script>
		<link rel="stylesheet" href="<?php echo URL;?>/public/css/stylesheet.css">
	</head>
	<body>
		<?php if(!Session::exist()){ ?>
		<div id="formWrapper">

			<div class="formWrapper">
				<div class="formTitle">Entrar</div>
				<form name="signIn" action="<?php echo URL; ?>User/login" method="post">
					<input name="username" required type="text" placeholder="Username" />
					<input name="password" required type="password" placeholder="Password" />
					<input id="signInBtn" type="submit" name="Entrar">
					<div class="smallText">
						<span>¿No estas registrado? <div class="button" id="signUpButton">Registrate Aquí</div></span>
						<span>¿Olvidaste tu password? <a href="">Recordar Password</a></span>
					</div>
				</form>
			</div>
			
			<div class="formWrapper hidden">
				<div class="formTitle">Registro</div>
				<form name="signUp" action="<?php echo URL;?>User/" method="post">
					<input name="name" type="text" required placeholder="Nombre" />
					<input name="username" type="text" required placeholder="Username" />
					<input name="password" type="password" required placeholder="Password" />
					<input name="email" type="email" required placeholder="e-mail" />
					<input id="signUpBtn" name="submitBtn" type="submit" name="Registrar">
					<div class="smallText">
						<span>¿Si estas registrado? <div class="button" id="signInButton">Volver</div></span>
					</div>
				</form>
			</div>

		</div>

		<script>

                        $(function(){
                            
                            $('#signUpButton').click(function(){
				$("form[name=signIn]").parent().hide();
				$("form[name=signUp]").parent().fadeToggle();
                            });

                            $('#signInButton').click(function(){
                                    $("form[name=signUp]").parent().hide();
                                    $("form[name=signIn]").parent().fadeToggle();
                            });
                            
                            $('#signUpBtn').click(function(e){
                                e.preventDefault();
                                var name = $('form[name=signUp] input[name=name]')[0].value;
                                var username = $('form[name=signUp] input[name=username]')[0].value;
                                var email = $('form[name=signUp] input[name=email]')[0].value;
                                var password = $('form[name=signUp] input[name=password]')[0].value;
                                signUp(name,username,email,password);
                            });
                            
                            $('#signInBtn').click(function(e){
                                e.preventDefault();
                                var username = $('form[name=signIn] input[name=username]')[0].value;
                                var password = $('form[name=signIn] input[name=password]')[0].value;
                            
                                signIn(username,password);
                            });
                        
                        });
                        
                        function signUp(name,username,email,password){
      
                            $.ajax({
                                type: "POST",
                                url: "<?php echo URL; ?>User/signUp",
                                data: {name: name, username:username, email: email, password:password}
                            }).done(function(response){
                                console.log(response);
                                if(response == "" ){
                                    alert("Registro Exitoso");
                                    signIn(username,password);
                                }else{
                                    alert("Error en el registro");
                                }
                            });
                            
                        }
                        
                        function signIn(username,password){
                            
                            
                            $.ajax({
                                type: "POST",
                                url: "<?php echo URL; ?>User/signIn",
                                data: {username:username, password:password}
                            }).done(function(response){
                              
                                if(response == 1){
                                    window.location.href = "<?php echo URL; ?>User/profile";
                                }else{
                                    alert("Usuario o Password Incorrecto");
                                }
                            });
                            
                        }
		</script>

		<?php }else{ ?>

				

		<?php } ?>
	</body>
</html>
