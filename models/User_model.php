<?php

    Class User_model extends Model{
        
        function __construct(){
            parent::__construct();
        }
        
        function signUp($data){
            
            return $this->db->insert('usuarios',$data);
        }
        
        function signIn($username,$password){
            
            return $this->db->check($username,'usuarios',
                                    "password = '".$password."'", true);
        }
        
        function select($fields,$where=''){
            return $this->db->select($fields,'usuarios',$where);
        }
    }

?>